require "rubygems"
require "bundler/setup"
require 'couchparty'

require 'awesome_print'

require 'time'
require 'json'

# return [ user, path]
def read_auth
  File.open('docker/.env').read.lines.map { |l| l.strip.split('=').last}
end



def create_db(server:, db_name:)


  #server.delete(db: db_name)
  db = server.create(db: db_name, options: {partitioned: true})

  # clean up security
  db.set_security

  index = '
      {
    "type": "json",
    "partitioned": true,
    "name" : "dt-index",
    "index": {
      "fields": ["dt"]
    }
}'

  index = JSON.parse(index)
  db.create_index(index)

  db
end

# add data
def add_data(db:)
  25_000.times do |i|
    doc = {
      _id: "#{rand(1000)}:#{i}",
      content: "hello_#{i}"
    }
    db.save_doc(doc, batch: 'ok')
  end
end

def update_data(db:)
  limit = 100
  block = 0
  while true
    options = {limit: limit, skip: block * limit, include_docs: true}

    query = db.all_docs(options: options)
    done = 0
    query["rows"].each do |doc|

      doc["doc"]['more']= 'more'
      db.save_doc(doc["doc"])
      done+=1

    end

    break if query["rows"].size == 0
    block += 1

    puts block
  end
end

name, password = read_auth
db_name = 'run_it'


server = CouchParty.server(url: 'http://localhost:5984', name: name, password: password)

#db = create_db(server: server, db_name: db_name)
db = server.db(db: db_name)

#add_data(db: db)
update_data(db: db)

#server.delete(db: db_name)

